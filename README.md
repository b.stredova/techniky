# Techniky

Techniky je možné zobrazit na https://narutolarp.gitlab.io/techniky/index.xml

Definice technik je v `public/techniky.xml`  
Zobrazování řeší `public/index.xml`  
A `public/index.html` pouze přesměruje na `public/index.xml` (protože gitlab pages nebere index.xml)

## Jak s technikami pracovat lokálně

Stačí pouze statický http server ve složce `public`:  
`cd public` a potom `python3 -m http.server` (nebo jakýkoli jiný http server)  
Poté bude výpis dostupný na adrese http://localhost:8000/index.xml

Změny provedené v `public/techniky.xml` se projeví po obnovení stránky (F5)

